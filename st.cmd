require essioc
require adsis8300bpm

# set macros
epicsEnvSet("CONTROL_GROUP",  "PBI-BPM01")
epicsEnvSet("AMC_NAME",       "Ctrl-AMC-130")
epicsEnvSet("AMC_DEVICE",     "/dev/sis8300-7")
epicsEnvSet("EVR_NAME",       "PBI-BPM01:Ctrl-EVR-101:")
epicsEnvSet("SYSTEM1_PREFIX", "MEBT-010:PBI-BPM-006:")
epicsEnvSet("SYSTEM1_NAME",   "MEBT 010 BPM 06")
epicsEnvSet("SYSTEM2_PREFIX", "MEBT-010:PBI-BPM-007:")
epicsEnvSet("SYSTEM2_NAME",   "MEBT 010 BPM 07")

# load BPM
iocshLoad("$(adsis8300bpm_DIR)/bpm-main.iocsh", "CONTROL_GROUP=$(CONTROL_GROUP), AMC_NAME=$(AMC_NAME), AMC_DEVICE=$(AMC_DEVICE), EVR_NAME=$(EVR_NAME)")

# load common module
iocshLoad $(essioc_DIR)/common_config.iocsh

